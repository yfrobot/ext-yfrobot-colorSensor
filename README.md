# YFROBOT 颜色传感器

![](./arduinoC/_images/featured.png)

## 简介 Introduction

本扩展库为Mind+软件设计。

支持 YFROBOT 颜色传感器。模块能够识别颜色，并获取 RGB 三基色颜色值！

注意：目前本用户库支持两种型号的颜色传感器，请根据型号选择。


## 相关链接 Links
* 本项目加载链接: https://github.com/YFROBOT-TM

* 产品购买链接: [YFROBOT商城](https://www.yfrobot.com/)、[YFROBOT淘宝商城](https://yfrobot.taobao.com/).


## 积木列表 Blocks
![](./arduinoC/_images/block.png)


## 示例程序 Examples
![](./arduinoC/_images/example.png)


## 许可证 License
MIT


## 硬件支持列表 Hardware Support
主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino uno        |             |       √已测试       |             | 
micro:bit        |             |       √已测试       |             | 
mpython掌控板        |             |        √已测试      |             | 
ESPONE        |             |        √已测试      |             | 
树莓派Pico        |             |        √未测试      |             | 


## 更新日志 Release Note
- V0.0.3  兼容MicroBit，掌控板，ESPONE主板，Mind+V1.7.3 RC3.0版本软件测试
- V0.0.2  优化veml6040颜色传感器 R G B值，Mind+V1.7.3 RC3.0版本软件测试
- V0.0.1  基础功能完成，Mind+V1.7.3 RC1.0版本软件测试


## 联系我们 Contact Us
* http://www.yfrobot.com.cn/wiki/index.php?title=%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC

## 其他扩展库 Other extension libraries
* http://yfrobot.com.cn/wiki/index.php?title=YFRobot%E5%BA%93_For_Mind%2B


## 参考 Reference Resources

