/** 
 * @file yfrobot
 * @brief YFROBOT's sensors Mind+ library.
 * @n This is a MindPlus graphics programming extension for YFROBOT's module.
 * 
 * @copyright    YFROBOT,2022
 * @copyright    MIT Lesser General Public License
 * 
 * @author [email](yfrobot@qq.com)
 * @date  2023-03-16
*/

enum SENSOR_TYPE {
  //% block="TCS"
  TCS,
  //% block="VEML"
  VEML
}

enum RGB_TYPE {
  //% block="R"
  R,
  //% block="G"
  G,
  //% block="B"
  B
}

//% color="#dfc243" iconWidth=40 iconHeight=40
namespace colorSensor {
  //% block="color sensor [TYPE] initliallize" blockType="command"
  //% TYPE.shadow="dropdown" TYPE.options="SENSOR_TYPE"
  export function init(parameter: any, block: any) {
    let type_sensor = parameter.TYPE.code;
    // let intpin = parameter.INTPIN.code;
    Generator.addInclude("includeWire", "#include <Wire.h>");
    if(type_sensor == "TCS"){
      Generator.addInclude("include_tcs3472x", `#include "Adafruit_TCS34725.h"`);
      Generator.addObject("tcs3472x_Object", "Adafruit_TCS34725", `tcs = Adafruit_TCS34725(); `);
      Generator.addSetup(`initSetup`, `tcs.begin();`);
    } else if(type_sensor == "VEML"){
      Generator.addInclude("include_veml6040", `#include "veml6040.h"`);
      Generator.addObject("veml6040_Object", "VEML6040", `vemlSensor; `);
      Generator.addSetup(`vemlSensor_initSetup`, `vemlSensor.begin();`);
      Generator.addSetup(`vemlSensor_setConfiguration`, `vemlSensor.setConfiguration(VEML6040_IT_40MS + VEML6040_AF_AUTO + VEML6040_SD_ENABLE);`);
      
    }
  }

  //% block="[TYPE] read color [RGB] value " blockType="reporter"
  //% TYPE.shadow="dropdown" TYPE.options="SENSOR_TYPE"
  //% RGB.shadow="dropdown" RGB.options="RGB_TYPE"
  export function readRGB(parameter: any, block: any) {
    let type_sensor = parameter.TYPE.code;
    let type_rgbvalue = parameter.RGB.code;
    if(type_sensor == "TCS"){
      if(type_rgbvalue == "R"){
        Generator.addCode(`tcs.getRed()`);
      } else if(type_rgbvalue == "G"){
        Generator.addCode(`tcs.getGreen()`);
      } else if(type_rgbvalue == "B"){
        Generator.addCode(`tcs.getBlue()`);
      }
    } else if(type_sensor == "VEML"){
      if(type_rgbvalue == "R"){
        Generator.addCode(`vemlSensor.getRed()`);
      } else if(type_rgbvalue == "G"){
        Generator.addCode(`vemlSensor.getGreen()`);
      } else if(type_rgbvalue == "B"){
        Generator.addCode(`vemlSensor.getBlue()`);
      }
    }
  }

  // //% block=" [TYPE] read raw color value [RGB]" blockType="reporter"
  // //% TYPE.shadow="dropdown" TYPE.options="SENSOR_TYPE"
  // //% RGB.shadow="dropdown" RGB.options="RGB_TYPE"
  // export function readRGBRaw(parameter: any, block: any) {
  //   let type_sensor = parameter.TYPE.code;
  //   let type_rgbvalue = parameter.RGB.code;
  //   if(type_sensor == "TCS"){
      
  //     Generator.addObject("tcs3472x_Object", "Adafruit_TCS34725", `tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X); `);
  //     if(type_rgbvalue == "R"){
  //       Generator.addCode(`tcs.getREDRaw()`);
  //     } else if(type_rgbvalue == "G"){
  //       Generator.addCode(`tcs.getGREENRaw()`);
  //     } else if(type_rgbvalue == "B"){
  //       Generator.addCode(`tcs.getBLUERaw()`);
  //     }
  //   } else if(type_sensor == "VEML"){

  //   }
  // }

}
